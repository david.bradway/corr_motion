"""
This example script computes the coherence as a function of angular lag. The transmit
apertures are realistic windowed plane waves, and the receive apertures are parabolic-
focused rectangular apertures.

Unlike the angular coherence function using ideal plane waves, windowed transmit
apertures are observed to be relatively insensitive to the receive aperture size.

Author:     Dongwoon Hyun (dongwoon.hyun@stanford.edu)
Created on: 2020-02-25
"""
# Code snippet to add python modules to path
if __name__ == "__main__" and (__package__ is None or __package__ == ""):
    from os import sys, path

    sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import numpy as np
from tqdm import tqdm
from apertures.RectAperture import RectAperture
from apertures.PlaneAperture import PlaneAperture
from corr_motion import corr_motion
import matplotlib.pyplot as plt

# Parameters
c = 1540
z = 20e-3
fc = 7e6
freqs = np.linspace(start=5, stop=9, num=7) * 1e6
bw = 0.6
pitchx = 0.2e-3
pitchy = 7.5e-3

# Number of elements in x, y for receive apertures (transmit aperture is infinite)
nelxT = 64
nelyT = 1
nelxRs = [16, 32, 64]
nelyR = 1

# Select angles to interrogate
dangle = 1 * np.pi / 180
maxangle = np.arctan2(max(nelxRs) / 2 * pitchx, z) * 1.2
lags = np.arange(0, np.ceil(maxangle / dangle) + 1)
lags = np.concatenate([-np.flip(lags[1:]), lags])
nlags = lags.size
anglags = lags * dangle * 180 / np.pi

# Loop through different receive aperture sizes
for nelxR in nelxRs:
    Tx0, Tx1 = np.array([-1, 1]) * nelxT * pitchx / 2
    Ty0, Ty1 = np.array([-1, 1]) * nelyT * pitchy / 2
    Rx0, Rx1 = np.array([-1, 1]) * nelxR * pitchx / 2
    Ry0, Ry1 = np.array([-1, 1]) * nelyR * pitchy / 2

    # Prepare output arrays (complex)
    G12 = np.zeros((nlags, freqs.size)) + 0j
    G11 = np.zeros((nlags, freqs.size)) + 0j
    G22 = np.zeros((nlags, freqs.size)) + 0j

    for ii, lag in enumerate(tqdm(lags)):
        # Loop through frequencies
        for i, f in enumerate(freqs):
            k = 2 * np.pi * f / c
            # Transmit and receive apertures
            T1 = PlaneAperture(Tx0, Tx1, Ty0, Ty1, -lag * dangle)
            T2 = PlaneAperture(Tx0, Tx1, Ty0, Ty1, +lag * dangle)
            R = RectAperture(Rx0, Rx1, Ry0, Ry1)
            # Compute the integrals
            G12[ii, i] = corr_motion([0, 0, 0], k, z, T1, R, T2, R)
            G11[ii, i] = corr_motion([0, 0, 0], k, z, T1, R, T1, R)
            G22[ii, i] = corr_motion([0, 0, 0], k, z, T2, R, T2, R)

    # Integrate over frequencies (i.e., make simulation broadband)
    F = np.expand_dims(np.exp(-4 * np.pi * ((freqs - fc) / (bw * fc)) ** 2), axis=0)
    G12bb = np.sum(G12 * F, axis=1, keepdims=False)
    G11bb = np.sum(G11 * F, axis=1, keepdims=False)
    G22bb = np.sum(G22 * F, axis=1, keepdims=False)

    g12 = G12bb / np.sqrt(np.abs(G11bb) * np.abs(G22bb))

    # Display plot
    plt.plot(anglags, np.real(g12), label="%d Rx" % nelxR)

plt.xlabel("Angular lag [degrees]")
plt.ylabel("Correlation coefficient")
plt.legend()
plt.ylim([-0.05, 1.05])
plt.xlim([min(anglags), max(anglags)])
plt.grid()
plt.show()

