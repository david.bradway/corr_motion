"""
This example script predicts the phase-shift estimate as well as its jitter for a plane
wave synthetic transmit aperture and rectangular focused receive aperture. In particular,
this example compares standard "sequential" synthetic transmit aperture versus
"interleaved" synthetic transmit apertures.

Results:
- The "interleaved" approach increases the aliasing limit, making it useful for detecting
  fast motions.
- The "interleaved" approach has similar jitter to the "sequential" approach.

Author:     Dongwoon Hyun (dongwoon.hyun@stanford.edu)
Created on: 2020-03-06
"""

# Code snippet to add python modules to path
if __name__ == "__main__" and (__package__ is None or __package__ == ""):
    from os import sys, path

    sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import numpy as np
from tqdm import tqdm
from apertures.PlaneAperture import PlaneAperture
from apertures.RectAperture import RectAperture
from corr_motion import corr_motion, jitter_decorr
import matplotlib.pyplot as plt

# Imaging parameters
c = 1540
z = 20e-3
freqs = np.linspace(start=5, stop=9, num=5) * 1e6

# Transducer parameters
fc = 7e6
bw = 0.6
pitchx = 0.2e-3
pitchy = 7.5e-3
nelx = 128
nely = 1
x0, x1 = np.array([-1, 1]) * nelx * pitchx / 2  # x limits
y0, y1 = np.array([-1, 1]) * nely * pitchy / 2  # y limits
angles = np.linspace(-0.1, 0.1, 3) * np.pi / 180  # Plane wave angles
N = angles.size

# Function to estimate the displacement and jitter
def estimate_disp(xd, yd, zd):
    d = np.array([xd, yd, zd]) / N
    # Receive aperture will always be focused array
    R = RectAperture(x0, x1, y0, y1)
    # Prepare output arrays for sequential acquisition
    G12s = np.zeros((freqs.size, N, N)) + 0j
    G11s = np.zeros((freqs.size, N, N)) + 0j
    # Prepare output arrays for interleaved acquisition
    G12i = np.zeros((freqs.size, N, N)) + 0j
    G11i = np.zeros((freqs.size, N, N)) + 0j
    # Loop through plane wave angles
    for m, a1 in enumerate(angles):
        T1 = PlaneAperture(x0, x1, y0, y1, a1)
        for n, a2 in enumerate(angles):
            T2 = PlaneAperture(x0, x1, y0, y1, a2)
            # Loop through frequencies
            for i, f in enumerate(freqs):
                k = 2 * np.pi * f / c
                # Compute displacements for sequential and interleaved acquisitions
                d12s = d * (n - m + N)
                d11s = d * (n - m + 0)
                d12i = d * (2 * (n - m) + 1)
                d11i = d * (2 * (n - m) + 0)
                # Compute integrals
                G12s[i, m, n] = corr_motion(d12s, k, z, T1, R, T2, R)
                G11s[i, m, n] = corr_motion(d11s, k, z, T1, R, T1, R)
                G12i[i, m, n] = corr_motion(d12i, k, z, T1, R, T2, R)
                G11i[i, m, n] = corr_motion(d11i, k, z, T1, R, T1, R)
    # Integrate over frequencies (i.e., make simulation broadband)
    F = np.exp(-4 * np.pi * ((freqs - fc) / (bw * fc)) ** 2)
    F = np.expand_dims(F, axis=(1, 2))
    G12bbs = np.sum(G12s * F)
    G11bbs = np.sum(G11s * F)
    G12bbi = np.sum(G12i * F)
    G11bbi = np.sum(G11i * F)
    g12bbs = G12bbs / np.abs(G11bbs)
    g12bbi = G12bbi / np.abs(G11bbi)
    # Compute phase shift (i.e., at center frequency)
    zd_est_seq = -np.angle(G12bbs) * c / (4 * np.pi * fc)
    zd_est_int = -np.angle(G12bbi) * c / (4 * np.pi * fc) * N
    # Compute jitter
    zd_jit_seq = jitter_decorr(np.abs(g12bbs)) * c / (4 * np.pi * fc)
    zd_jit_int = jitter_decorr(np.abs(g12bbi)) * c / (4 * np.pi * fc) * N

    # return zd_est_seq, zd_jit_seq, 0, 0
    return zd_est_seq, zd_jit_seq, zd_est_int, zd_jit_int


# List of displacements
d_list = np.linspace(0, 100, 41) * 1e-6
ndisps = d_list.size
# Versus azimuthal displacement
xd = 0
yd = 0
est_s = np.zeros((ndisps,))
jit_s = np.zeros((ndisps,))
est_i = np.zeros((ndisps,))
jit_i = np.zeros((ndisps,))
for i, zd in enumerate(tqdm(d_list)):
    est_s[i], jit_s[i], est_i[i], jit_i[i] = estimate_disp(xd, yd, zd)


# Display plot
d_list *= 1e6
est_s *= 1e6
jit_s *= 1e6
est_i *= 1e6
jit_i *= 1e6
aliaslim = c / fc / 4 * 1e6 * 1.3

plt.figure()
plt.errorbar(d_list, est_s, jit_s, linewidth=2, label="Sequential")
plt.errorbar(d_list, est_i, jit_i, linewidth=1, label="Interleaved", color="g")

plt.title("Motion Estimation with Plane Wave Synthetic Aperture")
plt.xlabel("Scatterer Motion [um]")
plt.ylabel("Axial Motion Estimate [um]")
plt.legend(loc="upper left")
plt.ylim([-aliaslim, aliaslim * 1.7])
plt.xlim([min(d_list), max(d_list)])
plt.grid()
plt.show()

