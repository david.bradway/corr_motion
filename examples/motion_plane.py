"""
This example script predicts the phase-shift estimate as well as its jitter for a plane
wave synthetic transmit aperture and rectangular focused receive aperture.

Results:
- Azimuthal motion increases jitter in the axial motion estimate.
- Axial motion changes the measured axial motion as expected and increases jitter.

Author:     Dongwoon Hyun (dongwoon.hyun@stanford.edu)
Created on: 2020-03-06
"""

# Code snippet to add python modules to path
if __name__ == "__main__" and (__package__ is None or __package__ == ""):
    from os import sys, path

    sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import numpy as np
from tqdm import tqdm
from apertures.PlaneAperture import PlaneAperture
from apertures.RectAperture import RectAperture
from corr_motion import corr_motion, jitter_decorr
import matplotlib.pyplot as plt

# Imaging parameters
c = 1540
z = 20e-3
freqs = np.linspace(start=5, stop=9, num=5) * 1e6

# Transducer parameters
fc = 7e6
bw = 0.6
pitchx = 0.2e-3
pitchy = 7.5e-3
nelx = 128
nely = 1
x0, x1 = np.array([-1, 1]) * nelx * pitchx / 2  # x limits
y0, y1 = np.array([-1, 1]) * nely * pitchy / 2  # y limits
angles = np.linspace(-1, 1, 3) * np.pi / 180  # Plane wave angles
N = angles.size

# Function to estimate the displacement and jitter
def estimate_disp(xd, yd, zd):
    d = np.array([xd, yd, zd]) / N
    # Receive aperture will always be focused array
    R = RectAperture(x0, x1, y0, y1)
    # Prepare output arrays (complex)
    G12 = np.zeros((freqs.size, N, N)) + 0j
    G11 = np.zeros((freqs.size, N, N)) + 0j
    # Loop through plane wave angles
    for m, a1 in enumerate(angles):
        T1 = PlaneAperture(x0, x1, y0, y1, a1)
        for n, a2 in enumerate(angles):
            T2 = PlaneAperture(x0, x1, y0, y1, a2)
            # Loop through frequencies
            for i, f in enumerate(freqs):
                k = 2 * np.pi * f / c
                # Compute the integrals
                G12[i, m, n] = corr_motion(d * (n - m + N), k, z, T1, R, T2, R)
                G11[i, m, n] = corr_motion(d * (n - m + 0), k, z, T1, R, T1, R)
    # Integrate over frequencies (i.e., make simulation broadband)
    F = np.exp(-4 * np.pi * ((freqs - fc) / (bw * fc)) ** 2)
    F = np.expand_dims(F, axis=(1, 2))
    G12bb = np.sum(G12 * F)
    G11bb = np.sum(G11 * F)
    g12bb = G12bb / np.abs(G11bb)
    # Compute phase shift (i.e., at center frequency)
    zd_est = -np.angle(G12bb) * c / (4 * np.pi * fc)
    # Compute jitter
    zd_jit = jitter_decorr(np.abs(g12bb)) * c / (4 * np.pi * fc)

    return zd_est, zd_jit


# List of displacements
d_list = np.linspace(0, 100, 41) * 1e-6
ndisps = d_list.size
# Versus azimuthal displacement
yd = 0
zd = 0
est_xd = np.zeros((ndisps,))
jit_xd = np.zeros((ndisps,))
for i, xd in enumerate(tqdm(d_list)):
    est_xd[i], jit_xd[i] = estimate_disp(xd, yd, zd)

# Versus axial displacement
xd = 0
yd = 0
est_zd = np.zeros((ndisps,))
jit_zd = np.zeros((ndisps,))
for i, zd in enumerate(tqdm(d_list)):
    est_zd[i], jit_zd[i] = estimate_disp(xd, yd, zd)

# Display plot
d_list *= 1e6
est_xd *= 1e6
jit_xd *= 1e6
est_zd *= 1e6
jit_zd *= 1e6
aliaslim = c / fc / 4 * 1e6 * 1.3

plt.figure()
plt.errorbar(d_list, est_xd, jit_xd, linewidth=2, label="Azimuthal Motion")
plt.errorbar(d_list, est_zd, jit_zd, linewidth=1, label="Axial Motion")

plt.title("Motion Estimation with Plane Wave Synthetic Aperture")
plt.xlabel("Scatterer Motion [um]")
plt.ylabel("Axial Motion Estimate [um]")
plt.legend()
plt.ylim([-aliaslim, aliaslim])
plt.xlim([min(d_list), max(d_list)])
plt.grid()
plt.show()

