"""
This example script computes the coherence as a function of spatial lag. The transmit and
receive apertures are all parabolic-focused rectangular apertures.

The spatial coherence function is shown to depend on the width of the transmit aperture.

Author:     Dongwoon Hyun (dongwoon.hyun@stanford.edu)
Created on: 2020-02-25
"""

# Code snippet to add python modules to path
if __name__ == "__main__" and (__package__ is None or __package__ == ""):
    from os import sys, path

    sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import numpy as np
from tqdm import tqdm
from apertures.RectAperture import RectAperture
from corr_motion import corr_motion
import matplotlib.pyplot as plt

# Parameters
c = 1540
z = 20e-3
fc = 7e6
freqs = np.linspace(start=5, stop=9, num=7) * 1e6
bw = 0.6
pitchx = 0.2e-3
pitchy = 7.5e-3

# Number of elements in x, y for transmit and receive apertures
nelxTs = [16, 32, 64]
nelyT = 1
nelxR = 1
nelyR = 1

# Loop through different transmit aperture sizes
for nelxT in nelxTs:
    Tx0, Tx1 = np.array([-1, 1]) * nelxT * pitchx / 2
    Ty0, Ty1 = np.array([-1, 1]) * nelyT * pitchy / 2
    Rx0, Rx1 = np.array([-1, 1]) * nelxR * pitchx / 2
    Ry0, Ry1 = np.array([-1, 1]) * nelyR * pitchy / 2
    lags = np.arange(-max(nelxTs), max(nelxTs) + 1)
    nlags = lags.size

    # Prepare output arrays (complex)
    G12 = np.zeros((nlags, freqs.size)) + 0j
    G11 = np.zeros((nlags, freqs.size)) + 0j
    G22 = np.zeros((nlags, freqs.size)) + 0j

    for ii, lag in enumerate(tqdm(lags)):

        xctrs = np.array([-0.5, 0.5]) * pitchx * lag

        # Loop through frequencies
        for i, f in enumerate(freqs):
            k = 2 * np.pi * f / c
            # Transmit and receive apertures
            T = RectAperture(Tx0, Tx1, Ty0, Ty1)
            R1 = RectAperture(Rx0 + xctrs[0], Rx1 + xctrs[0], Ry0, Ry1)
            R2 = RectAperture(Rx0 + xctrs[1], Rx1 + xctrs[1], Ry0, Ry1)
            # Compute the integrals
            G12[ii, i] = corr_motion([0, 0, 0], k, z, T, R1, T, R2)
            G11[ii, i] = corr_motion([0, 0, 0], k, z, T, R1, T, R1)
            G22[ii, i] = corr_motion([0, 0, 0], k, z, T, R2, T, R2)

    # Integrate over frequencies (i.e., make simulation broadband)
    F = np.expand_dims(np.exp(-4 * np.pi * ((freqs - fc) / (bw * fc)) ** 2), axis=0)
    G12bb = np.sum(G12 * F, axis=1, keepdims=False)
    G11bb = np.sum(G11 * F, axis=1, keepdims=False)
    G22bb = np.sum(G22 * F, axis=1, keepdims=False)

    g12 = G12bb / np.sqrt(np.abs(G11bb) * np.abs(G22bb))

    # Display plot
    plt.plot(lags, np.real(g12), label="%d Tx" % nelxT)

plt.xlabel("Spatial lag [# pitch]")
plt.ylabel("Correlation coefficient")
plt.legend()
plt.ylim([-0.05, 1.05])
plt.xlim([min(lags), max(lags)])
plt.grid()
plt.show()

