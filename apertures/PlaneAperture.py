import numpy as np
from apertures.RectAperture import RectAperture


class PlaneAperture(RectAperture):
    def __init__(self, xlim0, xlim1, ylim0, ylim1, xdir=0, ydir=np.nan):
        # Run initialization step of RectAperture
        super().__init__(xlim0, xlim1, ylim0, ylim1)
        # Run plane-wave-specific components
        self.xdir = xdir  # Azimuthal angle
        self.ydir = ydir  # Elevation angle

    ## Make 2D Fourier transformed aperture functions for plane wave transmit.
    def fourier_transform(self, k, z):
        # Get the Fourier transform of the window function (i.e., of RectAperture)
        Wu, Wv = super().fourier_transform(k, z)

        # Use the principle of stationary phase, weighted by window function
        def fu(u):
            # If self.xdir is nan, just return RectAperture window function
            if np.isnan(self.xdir):
                return Wu(u)
            # Find the value of x where the phase is stationary for all u
            x0 = z * (np.sin(self.xdir) + 2 * np.pi * u / k)
            phi0 = x0 * (k * np.sin(self.xdir) + 2 * np.pi * u - k * x0 / z / 2)
            return np.convolve(Wu(u), np.exp(-1j * phi0), "same")

        def fv(v):
            # If self.ydir is nan, just return RectAperture window function
            if np.isnan(self.ydir):
                return Wv(v)
            # Find the value of y where the phase is stationary for all v
            y0 = z * (np.sin(self.ydir) + 2 * np.pi * v / k)
            phi0 = y0 * (k * np.sin(self.ydir) + 2 * np.pi * v - k * y0 / z / 2)
            return np.convolve(Wv(v), np.exp(-1j * phi0), "same")

        # Return the Fourier transforms as functions of spatial frequencies u, v
        return fu, fv
