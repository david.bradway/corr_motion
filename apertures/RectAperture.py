import numpy as np


class RectAperture:
    """ Rectangular aperture class.
    This class describes a rectangular aperture and creates requested aperture functions.
    """

    def __init__(self, xlim0, xlim1, ylim0, ylim1):
        # Limits of aperture in x, y
        self.x0 = xlim0  # xlim0 - Lower limit of aperture in x
        self.x1 = xlim1  # xlim1 - Upper limit of aperture in x
        self.y0 = ylim0  # ylim0 - Lower limit of aperture in y
        self.y1 = ylim1  # ylim1 - Upper limit of aperture in y
        # Center of aperture in x, y
        self.xc = (xlim0 + xlim1) / 2
        self.yc = (ylim0 + ylim1) / 2
        # Width of aperture in x, y
        self.xw = np.abs(xlim1 - xlim0)
        self.yw = np.abs(ylim1 - ylim0)
        # Separable in x and y
        self.separable = True

    ## Make 2D Fourier transformed aperture functions for parabolic focusing
    def fourier_transform(self, k, z):
        j2p = 2j * np.pi
        # Create lambda functions to evaluate Fourier transform
        #   fu - Fourier-transformed aperture as a function of parameter u
        #   fv - Fourier-transformed aperture as a function of parameter v
        fu = lambda u: np.exp(-j2p * u * self.xc) * self.xw * np.sinc(u * self.xw)
        fv = lambda v: np.exp(-j2p * v * self.yc) * self.yw * np.sinc(v * self.yw)
        return fu, fv

    ## Get maximum spatial frequencies and minimum increments for integration over u, v
    def spat_freqs(self, k, z, xd, yd, nzu, nzv, dzu, dzv):
        # Convert number of zero-crossings (nzu, nzv) to spatial frequencies
        umax = nzu / self.xw
        vmax = nzv / self.xw
        # Convert increment in zero-crossings (dzu, dzv) to spatial frequencies
        uinc = dzu / self.xw
        vinc = dzv / self.xw
        # Account for shift due to scatterer motion
        umax += np.abs(xd) * k / (2 * np.pi * z)
        vmax += np.abs(yd) * k / (2 * np.pi * z)
        return umax, vmax, uinc, vinc

